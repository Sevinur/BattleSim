coffee = require 'coffee-script'
path = require('path').resolve(__dirname, '../bw/pokemon.coffee')
eval(coffee.compile(require('fs').readFileSync(path, 'utf8'), bare: true))

@Pokemon::canMegaEvolve = ->
  if @species == "Rayquaza"
    hasDA = _.findWhere(@moves, {name: "Dragon Ascent"})
    console.log(hasDA)
    if typeof hasDA != "undefined"
      return true
  return false  if !@hasItem()
  return false  if @item.type != 'megastone'
  [ species, forme ] = @item.mega
  return false  if @species != species || @forme != 'default'
  return false  if @team.hasMegaEvolved and forme is "mega"
  #return false  if @team.hasPrimalEvolved and forme is "primal"
  return true

oldBlockSwitch = @Pokemon::blockSwitch
@Pokemon::blockSwitch = ->
  oldBlockSwitch.apply(this, arguments)  if !@hasType("Ghost")

oldHasTakeableItem = @Pokemon::hasTakeableItem
@Pokemon::hasTakeableItem = ->
  return false  if oldHasTakeableItem.apply(this, arguments) == false
  if @item.type == 'megastone'
    [ species, forme ] = @item.mega
    return false  if @species == species
  else if @item.type is "formeitem"
    [ species, forme ] = @item.itemForme
    return false  if @species == species
  return true

# Powder moves no longer affect Grass-type Pokemon.
oldShouldBlockExecution = @Pokemon::shouldBlockExecution
@Pokemon::shouldBlockExecution = (move, user) ->
  if move.hasFlag("powder") && @hasType("Grass")
    move.fail(@battle, user)
    return true
  oldShouldBlockExecution.apply(this, arguments)

# In XY, Stance Change is another specially hardcoded ability that cannot change
oldHasChangeableAbility = @Pokemon::hasChangeableAbility
@Pokemon::hasChangeableAbility = ->
  !@hasAbility("Stance Change") && oldHasChangeableAbility.call(this)

oldgetMaxLevel = @Pokemon::getMaxLevel
@Pokemon::getMaxLevel = ->
  oldgetMaxLevel.apply(this, arguments)
  level = 120
  console.log(level)
  level